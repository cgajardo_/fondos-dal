<?php
class Tazos_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function transfer(){
		$rs = $this->db->query("SELECT * FROM fondos_excel");
		$rows = $rs->result_array();
		
		foreach ($rows as $row){
			$tipo_id = explode('.', $row['Tipodeprestacion'])[0];
			$estado_id = explode('.', $row['Estadodelinstrumento'])[0];
			$tipo_cooperacion_id = explode('.', $row['TipodeCooperacion'])[0];
			$alcance_id = explode('.', $row['Alcance'])[0];
			$zona_id = explode('.', $row['Zona'])[0];
			$ambito_id = explode('.', $row['Ambito'])[0];
			$destinatario_id = explode('.', $row['Resumendestinatarios'])[0];
			$destinatario_id2 = explode('.', $row['Resumendestinatarios2'])[0];
			
			// Conversion en fecha
			$ultima_revision = strtotime($row['Ultimaactualizacion']);
			
			//Insert en institucion
			$result = $this->db->get_where("institucion", array("nombre" => $row["Institucion"]));
			
			$institucion_id = 0;
			if($result->num_rows() > 0){
				$arr = $result->row_array();
				$institucion_id = $arr['id'];
			}else{
				$this->db->insert("institucion", array(
						"nombre"=>$row["Institucion"],
						"area"=>$row["Departamentooarea"],
						"direccion"=>$row['Direccion'],
						"web"=>$row['Paginaweb'],
						"mail"=>$row['Mail'],
						"telefono"=>$row['Telefono'] ));
				
				$institucion_id = $this->db->insert_id();
			}
			
			
			//Insertar una Territoriliadad
			if($zona_id == "")
				$zona_id = 0;
			
			$this->db->insert("territorialidad", 
				array(
					"id_alcance"=>$alcance_id,
					"id_zona"=>$zona_id )
			);
			
			$territorilidad_id = $this->db->insert_id();
			
			// TODO: Asociamos la territorialidad a las regiones
			
			// Reunimos los tiempos de acceso
			$tiempo_acceso = "";
			
			if($row["Tiempomaximodeejecucion"] != "")
				$tiempo_acceso = $row["Tiempomaximodeejecucion"];
			else 
				$tiempo_acceso = $row["Tiempodeacceso"];
			
			// Reunimos los medios
			$medios = "";
			if($row["Atravesdequemedios"] != "")
				$medios = $row["Atravesdequemedios"];
			else
				$medios = $row["Mediosdecomunicacion"];
			
			//Reunimos los plazos máximo
			$plazo_respuesta = "";
			if($row["Plazomaximodeesperaderespuesta"] != "")
				$plazo_respuesta = $row["Plazomaximodeesperaderespuesta"];
			else
				$plazo_respuesta = $row["Plazomaximodeespera"];
			
			// Vinculo con la pobreza
			$vinculo_id = 0;
			switch ($row["Vinculoconpobreza"]) {
				case "Asociado":
					$vinculo_id = 2;
				break;
				case "Complementario":
					$vinculo_id = 3;
				break;
				case "Centrado":
					$vinculo_id = 1;
				break;
			}
			
			//recuperar el ID de institucion
			$valores = array(
					"nombre"=>$row['Nombre'],
					"tipo_fondo_id"=>$tipo_id,
					"estado_id"=>$estado_id,
					"descripcion"=>$row['Descripcion'],
					"institucion_id"=>$institucion_id,
					"tipo_cooperacion_id"=>$tipo_cooperacion_id,
					"destinatarios_requisitos_texto"=>$row["Destinatarios"].";;".$row["Requisitos"].";;".$row["RequisitosR"].";;Documentos: ".$row["Documentosnecesarios"],
					"territorialidad_id"=>$territorilidad_id,
					"region_text"=>$row["Region"],
					"ultima_revision"=>$ultima_revision,
					"mecanismo_seleccion"=>$row["Tienemecanismodeseleccion"],
					"instrumento_seleccion"=>$row["Instrumentodeseleccion"],
					"requiere_postulacion" => $row["Hayquepostularparaacceder"],
					"plazo_postulacion" => $row["Plazodepostulacion"],
					"plazo_espera"=>$plazo_respuesta,
					"tiempo_acceso" => $tiempo_acceso,
					"tiene_difusion" => $row["Tienecontempladodifusion"],
					"campaña"=> $row["Campanas"],
					"pagina_informacion" => $row["Paginaweb2"],
					"comentarios" => $row["Otro"],
					"observaciones" => $row["Observaciones"],
					"vinculo_id" => $vinculo_id,
					"ambito_id" => $ambito_id,
					"subambito_text" => $row["Subambito"],
					"tiempo_acceso"=> $row["Subambito"],
					"informacion_disponible" => $row["Lainformacionestadisponibleenlasdistintasinstanciasdeejecucion"],
					"telefonos_informacion" => $row["Telefonosdeinformacion"],
					"requisito_edad_text" => $row["Requisitodeedad"],
					"origen_financiamiento" => $row["Origendelfinanciamientodelainstitucion"],
					"origen_financiamiento_prestacion" => $row["Origendelfinanciamientodelaprestacion"],
					"categoria" => $row["Tipo"],
					"empresa_relacionada" => $row["Empresarelacionada"]
					
			);
			
			if($tipo_cooperacion_id <= 0)
				unset($valores["tipo_cooperacion_id"]);
			
			if($row["Hayquepostularparaacceder"] == "")
				unset($valores["requiere_postulacion"]);
			
			if($row["Tienecontempladodifusion"] == "")
				unset($valores["Tienecontempladodifusion"]);
			
			$this->db->insert("fondo", $valores);
			//ID del fondo recien insertado
			$fondo_id = $this->db->insert_id();
			
			
			//Insercion de fondo, destinatario
			if($destinatario_id != ""){
				$this->db->insert("fondo_destinatario",
					array(
						"id_fondo" => $fondo_id,
						"id_destinatario" => $destinatario_id )
				);
			};
			
			if($destinatario_id2 != ""){
				$this->db->insert("fondo_destinatario",
						array(
								"id_fondo" => $fondo_id,
								"id_destinatario" => $destinatario_id2 )
				);
			};
			
			
		}		
		return 'ok' ;
	}
}
?>