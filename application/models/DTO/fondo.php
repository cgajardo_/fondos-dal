<?php
class Fondo extends CI_Model{

	public $id;
	public $nombre;
	public $descripcion;
	public $plazo_postulacion;
	public $requisitos;
	public $imagen;

	public function __construct($ID = NULL, $NOMBRE = NULL, $DESCRIPCION = NULL, 
			$PLAZO = NULL, $REQUISITOS = NULL, $IMAGEN = NULL){
		$this->id = $ID;
		$this->nombre = $NOMBRE;
		$this->descripcion = $DESCRIPCION;
		$this->plazo_postulacion = $PLAZO;
		$this->requisitos = $REQUISITOS;
		$this->imagen = $IMAGEN;
	}

	public function __toString(){
		return $this->nombre.':('.(string)$this->nombre.')';
	}

}