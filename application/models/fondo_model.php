<?php
class Fondo_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getAll(){
		$rs = $this->db->query("SELECT * FROM fondo WHERE id > 3455");
		$rows = $rs->result_array();
		
		$fondos = array();
		
		foreach ($rows as $row){
			array_push($fondos, new Fondo($row['id'], $row['nombre'], $row['descripcion'], $row['plazo_postulacion'], explode(";;", $row['destinatarios_requisitos_texto']),
			$row['imagen_fondo']));
		}		
		return $fondos ;
	}
	
	public function getRandomLimited($cantidad){
		$rs = $this->db->query("SELECT * FROM fondo WHERE id > 3455 ORDER BY RAND() LIMIT ".$cantidad);
		$rows = $rs->result_array();
		
		$fondos = array();
		
		foreach ($rows as $row){
			array_push($fondos, new Fondo($row['id'], $row['nombre'], $row['descripcion'], $row['plazo_postulacion'], explode(";;", $row['destinatarios_requisitos_texto']),
			$row['imagen_fondo']));
		}
		
		return $fondos ;
	}
	
	public function getFiltered($filtros){
		$query_string = "SELECT f.* FROM fondo f 
				LEFT JOIN fondo_destinatario fd ON fd.id_fondo = f.id 
				LEFT JOIN territorialidad t on t.id = f.territorialidad_id 
				WHERE f.id > 3455 AND 1=1";
		
		foreach ($filtros as $filtro){
			$query_string .=" AND ".$filtro['columna']." = ".$filtro['valor']." ";
		}
		$query_string .= " ORDER BY RAND() LIMIT 12";
		
		$rs = $this->db->query($query_string);
		$rows = $rs->result_array();
		
		$fondos = array();
		
		foreach ($rows as $row){
			array_push($fondos, new Fondo($row['id'], $row['nombre'], $row['descripcion'], $row['plazo_postulacion'], explode(";;", $row['destinatarios_requisitos_texto']),
			$row['imagen_fondo']));
		}
		return $fondos ;
	}

}

?>
