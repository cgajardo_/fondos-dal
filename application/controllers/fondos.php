<?php
class Fondos extends CI_Controller {
	
	public function filtrados(){
		
		//$data['fondos'] = $this->fondo_model->getAll();
// 		$filtro = $this->input->get('filtro');
// 		$data['fondos'] = $this->fondo_model->ge
// 		$this->load->view('fondos/index',$data);

		return $this->filter();
	}
	
	
	//cgajardo: lista todos los fondos existentes
	public function index()
	{
		//$data['fondos'] = $this->fondo_model->getAll();
		$data['fondos'] = $this->fondo_model->getRandomLimited(12);
		$this->load->view('fondos/index',$data);

	}

	
	public function fix(){
		echo $this->tazos_model->transfer();
		//echo "nada";
	}
	
	public function filter(){
		
		$filtros = array();
		
		if($this->input->get('tipo')!==FALSE)
			array_push($filtros, array('nombre'=>'tipo', 'valor'=>$this->input->get('tipo'), 'columna'=>'f.tipo_fondo_id'));
		
		if($this->input->get('vinculo')!==FALSE)
			array_push($filtros, array('nombre'=>'vinculo', 'valor'=>$this->input->get('vinculo'), 'columna'=>'f.vinculo_id'));
		
		if($this->input->get('ambito')!==FALSE)
			array_push($filtros, array('nombre'=>'ambito', 'valor'=>$this->input->get('ambito'), 'columna'=>'f.ambito_id'));
		
		if($this->input->get('destinatario')!==FALSE)
			array_push($filtros, array('nombre'=>'destinatario', 'valor'=>$this->input->get('destinatario'), 'columna'=>'fd.id_destinatario'));
		
		if($this->input->get('alcance')!==FALSE)
			array_push($filtros, array('nombre'=>'alcance', 'valor'=>$this->input->get('alcance'), 'columna'=>'t.id_alcance'));
		
		if($this->input->get('zona')!==FALSE)
			array_push($filtros, array('nombre'=>'zona', 'valor'=>$this->input->get('zona'), 'columna'=>'t.id_zona'));
		
		if($this->input->get('estado')!==FALSE)
			array_push($filtros, array('nombre'=>'estado', 'valor'=>$this->input->get('estado'), 'columna'=>'f.estado_id'));
		
		$data['fondos'] = $this->fondo_model->getFiltered($filtros);
		$data['filtros'] = $filtros;
		
		//$this->load->view('fondos/filtrados',$data);
		$this->load->view('fondos/index',$data);
	}
}