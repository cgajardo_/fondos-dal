<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lector extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	function leerExcel()
	{
		$this->load->model('lector_model');
        $this->load->helper('dom');
		$html = file_get_html('files/ues.html');
		
			$tabla = $html->find('table',0);
	$columnas = $tabla->find('tr',1);
	$headers = array();
	foreach($columnas->find('td') as $td)
		$headers[] = strip_tags($td->innertext);
		
		$k = 0;
		echo "CREATE TABLE fondos_excel (";
	foreach($headers as $h){
		if($k){
			if($k == 1)
				echo $h." VARCHAR(1024) ";
			else
				echo ", $h VARCHAR(1024)";
		}
		$k++;
	}
	
	echo ")";
	
	for($i = 2; $i++; true){
		$tr = $tabla->find('tr',$i);
		if(!$tr)
			break;
			
		$fila = array();
		$j = 0;
		foreach($tr->find('td') as $td){
			if($j > 1 and $headers[$j] and $headers[$j] !== '')
			$fila[$headers[$j]] = strip_tags($td->innertext);
			$j++;
		}
		$this->lector_model->insertar_fila($fila);
		print_r($fila);
		echo "<br/>";
	}

        $data['html'] =  $html;
        $this->load->view('lector/index', $data);  
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */ 
