<body>
<div id="template-wrapper">
    <div id="module-container"><!-- start #module-container -->
        <div id="module-background-holder">   
            <div id="module-background-solid1" class="opacity_0"></div>     
        </div>
        <div id="module-container-holder" class="module-position-lc"  data-id="module-position-lc">
            <div id="module-full-width-gallery"  class="module-full-width-gallery " > <!-- .shadow-side-all-->
                <div id="module-full-width-holder">
                <?php foreach ($fondos as $key => $fondo):?>
                	
                	<div class="full-width-item fondo_caja_foto" onclick="cambiarDescripcion(this);">
                		<input type="hidden" name="fondo_descripcion" value="<?php echo $fondo->descripcion;?>">
                		<input type="hidden" name="fondo_nombre" value="<?php echo $fondo->nombre;?>">
                		<input type="hidden" name="fondo_id" value="<?php echo $fondo->id;?>">
                		<?php if($fondo->imagen != ""):?>
                			<img src="./public/assets/media<?php echo $fondo->imagen;?>" class="opacity_0" onload="animateThumb(this)" alt=""  />
                		<?php else: ?>
                			<img src="./public/assets/media/portfolio/thumbs/thumb<?php echo rand(1, 9);?>.jpg" class="opacity_0" onload="animateThumb(this)" alt=""  />
                		<?php endif;?>
                        <div id="thumb-image-hover" class="hover-default">
                            <div class="background opacity_6"></div>
                            <div class="zoom-gallery"></div>
                            <div class="item-title"><p><?php echo $fondo->nombre;?></p></div>
                        </div>                   
                    </div>
                <?php endforeach;?>
                </div>             
            </div>
            
        </div>
        <div id="module-scrollbar-holder_v2">
            <div id="module-scrollbar-background" class="opacity_8"></div>
            <div id="module-scrollbar-dragger"></div>
        </div>	
        
        <div id="full-width-preview"><!-- start .full-width-preview" -->
            <div class="full-width-preview-background opacity_9_7"></div>
            
            <div id="full-width-preview-media-holder">
                <div class="full-width-preview-media-holder-background">
	                <h1 id="fondo_detalle_nombre" ></h1>
	                <h2 id="fondo_detalle_descripcion"></h2>
	                
                </div>
                
                  <!-- ignacio: el preload se quedaba pegado, 
                  				lo desactive desde css. 
                  				Si lo borro todo falla -->
                <div class="full-width-preview-media-loader"></div>           
            </div>
            
            <div class="preview-arrow-close">
                <div class="preview-arrow-backg opacity_2"></div>
                <div class="preview-arrow-close-sign"></div>
            </div>
            
            <!-- ignacio: no se que es eso de preview-smartphone-info. 
            	 Ante la duda lo dejo intacto -->
			<div class="preview-smartphone-info">
                <div class="preview-smartphone-info-backg opacity_1"></div>
                <span class="show_info">SHOW INFO</span>
                <span class="hide_info">HIDE INFO</span>
            </div>
						
            <div id="full-width-preview-info-holder" class="module-position-rc">
                <div id="full-width-info-container" class="full-width-info-container shadow-side-left">
                    <div class="full-width-info-holder">
                    	<?php foreach ($fondos as $key => $fondo): ?>
                        <div class="full-width-info-holder-desc">
                            <p><?php echo $fondo->nombre;?></p>
                            <div class="custom-separator"></div>
                            <span>Destinatarios: <ul><?php 
                            foreach ($fondo->requisitos as $requisito){
								if($requisito!="Documentos: ") 
                            		echo "<li>".$requisito."</li>";
							}?></ul></span>
                            <div class="custom-separator"></div>
                            <span>Fecha de postulacion: <?php if($fondo->plazo_postulacion != "") echo $fondo->plazo_postulacion;else echo "no declarada.";?></span>
							<!-- start social facebook, twitter and pinterest -->
                            <div id="social-holder">
                                <div id="social-holder-back" class="opacity_2"></div>
                                <div data-src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fthemes.mediacreed.com%2F%23portfolio&amp;send=false&amp;layout=button_count&amp;width=85&amp;show_faces=false&amp;font=verdana&amp;colorscheme=light&amp;action=like&amp;height=21" 
                                     style="float:left; border:none; overflow:hidden; width:82px; height:21px; margin: 6px 5px 5px 10px;"></div>
                                <div data-src="https://platform.twitter.com/widgets/tweet_button.html?url=http://themes.mediacreed.com/html/synergy/#portfolio&text=Synergy - Awesome HTML Portfolio Template&via=mediacreed" 
                                     style="float:left; border:none; overflow:hidden; width:82px; height:21px; margin: 6px 5px 5px 10px;"></div>     
                                <a href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fthemes.mediacreed.com%2Fhtml%2Fsynergy%2F&media=http%3A%2F%2Fthemes.mediacreed.com%2Fdesc_images%2Fprofile_preview_synergy.jpg&description=Synergy%20is%20a%20responsive%20%26%20interactive%20fullscreen%20portfolio%20for%20artists%2C%20photographers%2C%20media%20agencies%2C%20restaurants%20and%20for%20everyone%20that%20wants%20to%20showcase%20their%20portfolio%20in%20a%20professional%20way." class="pin-it-button" count-layout="horizontal" target="_blank"><img border="0" src="//assets.pinterest.com/images/PinExt.png" title="Pin It" /></a>                                
                            </div>
                            <!-- end social facebook, twitter and pinterest -->
						</div>
						<?php endforeach;?>                      
                    </div>
                </div>        
            </div>        
        </div><!-- end .full-width-preview" -->
    </div><!-- end #module-container -->
</div>
    
</body>
</html>
<?php
print_r($fondos);
?>
