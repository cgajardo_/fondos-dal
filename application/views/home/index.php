﻿<?php 
$this->load->helper('url');
$this->load->library('tank_auth');
?>
<!DOCTYPE html>
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,IE=9,IE=8,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="base_url" content="<?php echo base_url();?>" />
    <meta itemprop="image" content="assets/favicon.png" />
    <link rel="icon" href="assets/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon" />
    
    
	<title>Synergy - Responsive and Interactive HTML Portfolio</title>
    
	<meta name="description" content="" />
	<meta name="author" content="MEDIACREED.COM" />
    <!--<meta name="fragment" content="!">-->
    
        <link rel="stylesheet" href="./public/css/style.css" />
        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic|PT+Sans+Caption:400,700' rel='stylesheet' type='text/css' />        
        
        <!-- SCRIPT IE FIXES -->  
        <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]--> 
        <!-- END SCRIPT IE FIXES-->
      
      
        <!-- START TEMPLATE JavaScript load -->
    	<script type="text/javascript" src="./public/js/libs/jquery-1.7.2.min.js"></script>    
        <script type="text/javascript" src="./public/js/libs/modernizr.custom.min.js"></script> 
        <script type="text/javascript" src="./public/js/libs/jquery.wipetouch.js"></script>   
        
        <script type="text/javascript" src="./public/js/libs/jquery.gmap.min.js"></script>
        <script type="text/javascript" src="./public/js/greensock/minified/TweenMax.min.js"></script>    
        <script type="text/javascript" src="./public/js/libs/jquery.timer.js"></script>
        <script type="text/javascript" src="./public/js/libs/jqueryui/1.8/jquery-ui.min.js"></script>
        <script type="text/javascript" src="./public/js/libs/jquery.mousewheel.min.js"></script>
		<script type="text/javascript" src="./public/js/audio-js/jquery.jplayer.min.js"></script>
        <script type="text/javascript" src="./public/js/audio-js/jplayer.playlist.min.js"></script>
        <script type="text/javascript" src="./public/js/mediacreed/scrollbar/mc.custom.list.js"></script>
        <script type="text/javascript" src="./public/js/mc.modules.animation.js"></script> 
        <link rel="stylesheet" href="./public/js/video-js/video-js.min.css" media="screen" />
        <script type="text/javascript" src="./public/js/video-js/video.min.js"></script>
        <!-- END TEMPLATE JavaScript load ---->
        
        <!--<script src="http://vjs.zencdn.net/c/video.js"></script>    
        Careful when using the online version because the destroy method throws an error.    
        Our version has the fix on destroy method. Until it updates we recommend using the JS file from the template.    
        -->
        <script>
            _V_.options.flash.swf = "js/video-js/video-js.swf";
            _V_.options.techOrder = ["html5", "flash", "links"];
            var params = {};
            params.bgcolor = "#000000";
            params.allowFullScreen = "true";       
            _V_.options.flash.params = params;
    
        </script>  
        <script type="text/javascript">
			function cambiarDescripcion(nodo){
				document.getElementById("fondo_detalle_nombre").innerHTML = nodo.querySelectorAll('[name=fondo_nombre]')[0].value;
				document.getElementById("fondo_detalle_descripcion").innerHTML = nodo.querySelectorAll('[name=fondo_descripcion]')[0].value;
			}
		</script>  
</head>

<body>
<div class="main-template-loader"></div>

<div id="template-wrapper">
    <div id="menu-container"><!-- start #menu-container -->        
        <div class="menu-content-holder"><!-- start .menu-content-holder -->
            <div class="menu-background"></div>
            <div id="template-logo" class="template-logo" data-href="#fondos"></div> 
            <div id="template-menu" class="template-menu" data-current-module-type="full_width_gallery" data-side="none" data-href="#fondos"><!-- start #template-menu -->
            	  <!-- MI PERFIL -->
            	  <div class="menu-option-holder">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text">
                        <a href="#">MI PERFIL</a>
                        <div class="menu-option-sign">+</div>
                    </div>
                    <div class="sub-menu-holder">
						<!--
                        <div class="sub-menu-option-holder" data-module-type="text_page" data-side="height">
                            <div class="sub-menu-option-background"></div>
                            <div class="sub-menu-option-text"><a href="#about_us.html" data-path-href="./public/html/about_us/">Datos Personales</a></div>
                        </div>
                        <div class="sub-menu-option-holder" data-module-type="text_page" data-side="none">
                            <div class="sub-menu-option-background"></div>
                            <div class="sub-menu-option-text"><a href="#philosophy.html" data-path-href="./public/html/about_us/">Documentos</a></div>
                        </div>
                        <div class="sub-menu-option-holder" data-module-type="text_page" data-side="height">
                            <div class="sub-menu-option-background"> </div>
                            <div class="sub-menu-option-text"><a href="#ethics.html" data-path-href="./public/html/about_us/">Fondos Postulados</a></div>
                        </div>
                        -->
                        <?php if($logged){
							?>
							<div class="sub-menu-option-holder" data-module-type="text_page" data-side="none">
								<div class="sub-menu-option-background"> </div>
								<div class="sub-menu-option-text"><a href="<?php echo base_url();?>auth/edit_profile/edit/<?php echo $user->id; ?>" data-path-href="">Datos Personales</a></div>
							</div>
							<div class="sub-menu-option-holder" data-module-type="text_page" data-side="none">
								<div class="sub-menu-option-background"> </div>
								<div class="sub-menu-option-text"><a href="<?php echo base_url();?>auth/logout" data-path-href="">Log Out</a></div>
							</div>
						<?php 
						}else{
							?>
							<div class="sub-menu-option-holder" data-module-type="text_page" data-side="none">
								<div class="sub-menu-option-background"> </div>
								<div class="sub-menu-option-text"><a href="<?php echo base_url();?>welcome/login" data-path-href="">Log In</a></div>
							</div>
							<?php
						}
						?>                       
                    </div>
                </div> <!-- END MI PERFIL -->
                
            	 <!-- FONDOS -->
            	 <div class="menu-option-holder" data-module-type="full_width_gallery" data-side="none">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#fondos" data-path-href="./public/html/portfolio/">FONDOS</a></div>
                </div>
            	 
            	  <!-- CATEGORIAS -->
            	  
            	  <div class="menu-option-holder filtros_menu" data-module-type="full_width_gallery" data-side="none" style="background-color: #c0392b;">
                    <div id="menu-option-background" class="sub menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#fondos/filtrados?tipo=1" data-path-href="">Fondos concursables</a></div>
                </div>
                
                 <div class="menu-option-holder filtros_menu" data-module-type="full_width_gallery" data-side="none" style="background-color: rgba(211, 84, 0,1.0);">
                    <div id="menu-option-background" class="sub menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#fondos" data-path-href="">TERCERA EDAD</a></div>
                </div>
                
                      	  <div class="menu-option-holder filtros_menu" data-module-type="full_width_gallery" data-side="none" style="background-color: rgba(230, 126, 34,1.0);">
                    <div id="menu-option-background" class="sub menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#fondos" data-path-href="">EDUCACION</a></div>
                </div>
                
                 <div class="menu-option-holder filtros_menu" data-module-type="full_width_gallery" data-side="none" style="background-color: rgba(243, 156, 18,1.0);">
                    <div id="menu-option-background" class="sub menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#fondos" data-path-href="">EMPRENDIMIENTO</a></div>
                </div>
                                
                 <div class="menu-option-holder filtros_menu" data-module-type="full_width_gallery" data-side="none" style="background-color: rgba(241, 196, 15,1.0);">
                    <div id="menu-option-background" class="sub menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#fondos" data-path-href="">VIVIENDA</a></div>
                </div>
            	  
            	  
            	    <div class="menu-option-holder" data-module-type="news" data-side="height">
                    <div id="menu-option-background" class="menu-option-background"> </div>
                    <div id="menu-option-text" class="menu-option-text"><a href="#news.html" data-path-href="./public/html/news/">DESTACADOS PARA TI</a>
                    </div>
                </div>
            	          
            </div><!-- end #template-menu -->  
            <div id="template-smpartphone-menu">
                <select>
                    <option value="#">Navigate to...</option>
                    <option value="#"> HOME +</option>
                        <option value="#home_layout_1.html">  - Home Layout 1</option>
                        <option value="#home_layout_2.html">  - Home Layout 2</option>
                        <option value="#home_layout_3.html">  - Home Layout 3</option>
                    <option value="#"> ABOUT US +</option>
                        <option value="#about_us.html">  - About us</option>
                        <option value="#philosophy.html">  - Philosophy</option>
                        <option value="#ethics.html">  - Ethics</option>
                        <option value="#careers.html">  - Careers</option>
                    <option value="#news.html"> NEWS</option>
                    <option value="#fondos"> PORTFOLIO</option>
                    <option value="#"> OUR PROJECTS +</option>
                        <option value="#4_columns_projects.html">  - 4 Columns Projects</option>
                        <option value="#3_columns_projects.html">  - 3 Columns Projects</option>
                        <option value="#2_columns_projects.html">  - 2 Columns Projects</option>
                    <option value="#showreel.html"> SHOWREEL</option>    
                    <option value="#"> GALLERIES +</option>
                        <option value="#image_gallery.html">  - Image Gallery</option>
                        <option value="#mixed_gallery.html">  - Mixed Gallery</option>
                    <option value="#"> FEATURES +</option>
                        <option value="#full_width_text_and_image.html">  - Full Width Text + Image</option>
                        <option value="#full_width_text_and_video.html">  - Full Width Text + Video</option>
                        <option value="#fullscreen_video.html">  - Fullscreen Video</option>
                        <option value="#pricing_tables.html">  - Pricing Table</option>  
                    <option value="#contact.html"> CONTACT</option>
                </select>
            </div>
       
        </div><!-- end .menu-content-holder-->
        
        <div id="menu-hider">
            <div id="menu-hider-background"></div>
            <div id="menu-hider-icon"></div>
        </div>
    </div><!-- end #menu-container -->

    <div id="module-container">
    </div>
    
</div>

<div id="load-container">
</div>

<div id="loading-animation">
	<img src="assets/loaders/loader.gif" width="16" height="11" alt="Synergy - loading animation"/>
</div>
<div id="footer-social-tooltip"></div>

<div id="console-log"></div>
<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script> 
</body>
</html>
